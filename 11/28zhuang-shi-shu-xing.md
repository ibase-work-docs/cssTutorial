* 字重font-weight

| 值 | 描述 |
| :--- | :--- |
| normal | 默认。定义标准的字符。 |
| bold | 定义粗体字符。 |
| bolder | 定义更粗的字符。 |
| lighter | 定义更细的字符。 |
| 100-200-300-400-500-600-700-800-900 | 定义由粗到细的字符。400 等同于 normal，而 700 等同于 bold。 |

* 斜体font-style:italic



* 下划线text-decoration:
* 指针cursor



