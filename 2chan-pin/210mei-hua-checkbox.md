```
//html
<div class="checkbox">
   <input type="checkbox" id="c">
   <label for="c">我很牛</label>
</div>
```

```css
//css
input{
   display: none;
}
.checkbox label{
   background: url("off.png") no-repeat;
   padding-left: 20px;
}
.checkbox input:checked+label{
   background: url("on.png") no-repeat;
}
```

[在线实例](https://gitee.com/frontendLol/cssSkill/tree/master/checkbox勾选框的美化)

点击checkbox在这两张图片之间切换

![](/assets/checkbox.png)

![](/assets/on.png)

