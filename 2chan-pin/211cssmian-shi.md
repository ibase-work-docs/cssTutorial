##### 1.css选择器的优先级别排序

##### 2.雪碧图的作用

* 原理：运用background-position

* 优势：减少HTTP请求数,提高加载性能

##### 3.自定义字体的使用场景

* 宣传/品牌/banner等固定文案
* 字体图标

```
npm install font-spider -g
```

##### 4.base64使用

* 原理：将图片变为文本,内嵌到css中使用

* 优势：

              1.减少http请求,增加加载性能

* 应用场景:小图片
* 劣势：体积会增大1/3左右

##### 5.伪类和伪元素的区别

* 伪类表示状态

eg:

```
p:hover{}
//表示鼠标悬停     
```

* 伪元素是真的元素

##### 6.如何美化checkbox

* label\[for\]和id
* 隐藏原生input
* :checked+label



