### 非布局样式

* 字体，字重，行高，颜色，大小

* 背景，边框

* 滚动，换行

* 粗体，斜体，下划线

> ## 1.字体的预备机制

```
//简书的方案
body{
    font-family:-apple-system,SF UI Text,Arial,PingFang SC,Hiragino Sans GB,Microsoft YaHei,
    WenQuanYi Micro Hei,sans-serif;
}
```

![](/assets/01.png)

实际使用的，看调试工具使用的是**微软雅黑**

![](/assets/02.png)

## 





