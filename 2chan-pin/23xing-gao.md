```
<div>
    <span class="c1">hello world</span>
</div>
```

```css
//css
.c1{
line-height: 60px;
}
span{
background: red;
}
div{
border: 1px solid #333;
}
```

## 1.内联元素在块元素中垂直居中,使用_**line-height**_

显示结果

![](/assets/行高.png)

## 2.vertical-align设置文字的垂直对齐属性

```
vertical-align:baseline
//默认基线对齐
```

eg:

```css
//css
span{
    background: red;
}
.c1{
    font-size: 20px;
}
.c2{
    font-size: 30px;
}
.c3{
    font-size: 40px;
}
```

```html
//html
<div>
    <span class="c1">第1</span>
    <span class="c2">第2</span>
    <span class="c3">第3</span>
</div>
```

![](/assets/vertical-align.png)

如何改为居中对齐,以下代码

```
span{vertical-align:middle}
```

![](/assets/middle.png)

## 3.图片与文字会有空隙 原理是什么

```
<div>
    <span>美女</span>
    <img src="images/01.png" alt="">
</div>
//css
div{background:pink}
```

![](/assets/图片空隙.png)

原因：将图片识别为文字，默认是基线对齐的，所以会有3px左右的偏差。

如何解决：将图片设为基线对齐

```
img{vertical-align:bottom}
```

![](/assets/解决.png)

