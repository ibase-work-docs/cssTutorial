* 背景颜色

* 渐变色背景

  [https://webgradients.com/](https://webgradients.com/)

* 多背景叠加

* 背景图片和属性\(雪碧图\)

* base64和性能优化

* 多分辨率适配

### 1.纯的背景色

* 1.1 hsla

```css
//色相，饱和度，明度，透明度
background:hsla(230,50%,50%,.6);
```

* 1.2rgba

```css
background:rgba(20,30,243,.5)
```

* 1.3背景的简写

```
background:color img repeat position;
```

## 2.雪碧图

```
background-position:x y
//去调整图片的位置
```

### 3.base64 一串文本

```
好处:减少http连接数

坏处:
1.文件的体积会增大 会增大1/3
2.解码难度增大

适用场合:只适合小图片
```

在项目中通过构建的方式,转换为base64

