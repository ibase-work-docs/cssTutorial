### 1.边框的简写

```
border:width style color
```

### 2.边框的连接\(三角形\)

```
//html
<div></div>
//css
div{
    width:0;
    height:0;
    border:15px solid transparent;
    border-top-color:#333;
    }
```

结果如下图所示

![](/assets/三角形.png)

