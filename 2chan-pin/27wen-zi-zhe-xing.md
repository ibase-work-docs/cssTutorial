* overflow-wrap\(word-wrap\)对不可分割的长单词换行

| 值 | 描述 |
| :--- | :--- |
| normal | 只在允许的断字点换行（浏览器保持默认处理）。 |
| break-word | 在长单词或 URL 地址内部进行换行。 |

* word-break属性规定自动换行的处理方法

```
word-break: normal|break-all|keep-all;
```

| 值 | 描述 |
| :--- | :--- |
| normal | 使用浏览器默认的换行规则 |
| break-all | 允许在单词里换行 |
| keep-all | 只能在词与词的空格之间换行 |

* white-space 内容是否换行

```
white-space:nowrap;
```

## 文字以省略号结尾

```
//超出内容隐藏
overflow: hidden;
//设置内容不换行
white-space: nowrap;
//溢出部分以省略号结尾
text-overflow: ellipsis;
```



