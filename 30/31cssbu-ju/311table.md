| table-cell | 元素会作为表格单元格显示\(类似td,th\) |
| :--- | :--- |
| table-row | 元素会作为一个表格行显示\(类似tr\) |

```html
<table>
    <tr>
        <td class="left">left</td>
        <td class="right">right</td>
    </tr>
</table>
```

```css
//css
table{width:600px;height:300px;border-collapse:collapse}
.left{background:red}
.right{background:yellow}
```

显示如下

![](/assets/table01.png)

如何用div达到以上效果

```HTML
//html
<div class="table">
    <div class="table-row">
        <div class="left table-cell">
            left
        </div>
        <div class="right table-cell">
            right
        </div>
    </div>
</div>
```

```css
//CSS
.table{
    display: table;
    width: 800px;
    height: 200px;
}
.table-row{
    display: table-row;
}
.table-cell{
    display: table-cell;
    vertical-align: middle;
}
.left{
    background: red;
}
.right{
    background: yellow;
}
```



