![](/assets/兼容性.jpg)

```
<div class="container">
    <div class="flex">flex</div>
    <div class="flex">flex</div>
    <div class="flex">flex</div>
    <div class="flex">flex</div>
</div>
```

```css
//css
.container{
            width:800px;
            height:200px;
            border:1px solid #333;
            display: flex;
}
.flex{
            flex:1;
            background:red;
            margin:10px;
}
```

//container里的四个div被分成四等分,如下图所示![](/assets/01.gif)

### 2.左边给固定宽度,右边自适应

```
<div class="container">
        <div class="left"></div>
        <div class="right"></div>
</div>
```

```css
        .container{
            display:flex;
            width:800px;
            height:200px;
            border:1px solid #333;
        }
        .left{
            display:flex;
            width:200px;
            background-color: red;
        }
        .right{
            background:yellow;
            flex:1;
        }
```

![](/assets/flex02.png)



