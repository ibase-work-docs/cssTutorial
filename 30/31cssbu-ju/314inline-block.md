```
<div class="container">
        <div class="left"></div>
        <div class="right"></div>
</div>
```

> Tip:将left,right设置为inline-block时,他们之间会有间隙,浏览器把它们当文字在处理,因为字体之间的空隙导致的空白;只要将container的font-size设置为0,就可以解决。

```css
        .container{
            font-size: 0;
            width:800px;
            border:1px solid #333;
        }
        .left{
            display: inline-block;
            width:300px;
            background:red;
            height:200px;
        }
        .right{
            display:inline-block;
            width:500px;
            background:yellow;
            height:200px;
        }
```



