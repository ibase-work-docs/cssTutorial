### 响应式设计和布局

* 在不同设备上正常使用
* 一般主要处理屏幕大小问题
* 主要方法:

   1.隐藏+折行+自适应空间

   2.具体方法:rem/viewport/media query

```css
 @media (max-width:640px){
            .left{
                display:none;
            }
}
```

https://gitee.com/frontendLol/cssSkill/tree/master/@media%E8%AE%A9%E5%AF%B9%E5%BA%94%E7%9A%84%E9%A1%B5%E9%9D%A2%E9%9A%90%E8%97%8F



