@media可以针对[不同的媒体类型](/0),定义不同的样式

![](/assets/media.png)

```
@media only screen and (max-width: 500px) {
    /*css code*/
}

@media screen and (min-width:300px) and (max-width:500px) {
    /* CSS 代码 */
}
```

| max-width | 定义输出设备页面最大的可见区域宽度 |
| :--- | :--- |
| max-height | 定义输出设备页面最大的可见区域高度 |



