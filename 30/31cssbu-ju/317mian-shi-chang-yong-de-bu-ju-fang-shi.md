### 1.常用的布局方式

* 1.表格布局
* 2.float+margin布局
* 3.inline-block布局\(将父元素的font-size设置为0\)
* 4.flexbox布局

### 2.position:absolute/fixed有什么区别?

* 前者相对最近的absolute/relative的父元素
* 后者相对屏幕\(viewport\)

### 3.display:inline-block的间隙

* 原因:字符间距
* 解决方案:

```
1.消灭字符标签与标签之间紧挨着 eg<div>hello</div><div>hello</div>
2.消灭间距    父元素的font-size设置为0
```

### 4.如何清除浮动

* overflow:hidden;
* 伪元素

```
.row:after{
    content:"";
    display:table;
    clear:both;
}
```

### 5.如何适配移动端的页面

* viewport
* rem/viewport/media 
* 设计上:隐藏 折行 自适应



