* box-shadow
* text-shadow
* border-radius
* background
* clip-path

#### 1.box-shadow

```
box-shadow:offsetX offsetY blur size color inset
```

#### 2.text-shadow

```
text-shadow:offsetX offsetY size color
```

```css
p{
     text-indent: 30px;
     text-shadow: 1px 1px 2px #333;
     font-family: STKaiti
}
```

#### 3.border-radius

```
border-radius:topLeft topRight bottomRight bottomLeft
```

```css
<div></div>
//css
div{
    width:200px;
    height:30px;
    border-bottom:1px solid #333;
    border-radius: 10px 10px 30px 20px;
}
```

![](/assets/border.png)

#### 4.clip-path

* clip-path:inset\(x,y\)裁剪矩形

```
//x,y  表示横坐标,和纵坐标,裁剪的位置
clip-path:inset(x,y)
```

* clip-path:circel\(radius at x y\) 裁剪圆形

```
//radius表示半径  x,y表示裁剪的坐标
```



