## [本节参考张鑫旭](http://www.zhangxinxu.com/wordpress/2012/09/css3-3d-transform-perspective-animate-transition/)

### 1.perspective透视

* CSS3 3D transform的**透视点是在浏览器的前方**

实现3d效果有两种写法

```
<div class="parent">
    <div class="child">
    </div>
</div>
```

```
//1.给父元素透视
.parent{
//保留3d效果
 transform-style:preserve-3d
 perspective:300px;
}
```

```
//2.用在当前动画元素上
.child{
     transform: perspective(600px) rotateY(45deg);
}
```

### 2.3D transform中有下面这三个方法：

* `rotateX( angle )`

* `rotateY( angle )`

* `rotateZ( angle )`

* 这三个样式可以帮助理解三位坐标

单杠运动是`rotateX`

![](/assets/rotateX.jpg)

钢管舞 rotateY

![](/assets/rotateY.jpg)

转盘rotateZ

### ![](/assets/rotateZ.gif)![](/assets/transform3d.png)3.translateZ帮你寻找透视位置

### 近大远小,translateZ\(\)的值设置的越小,视觉上里我们就越远

在线demo

[https://gitee.com/frontendLol/cssJiaoCheng/blob/master/04translateZ.html](https://gitee.com/frontendLol/cssJiaoCheng/blob/master/04translateZ.html)

