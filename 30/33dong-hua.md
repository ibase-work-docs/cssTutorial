动画的原理：

1.视觉暂留作用

2.画面逐渐变化

动画的作用-愉悦感

### css中的动画类型

1.transition补间动画——从一种状态变成另一种状态

2.keyframe关键帧动画

3.逐帧动画

## 1.transition

```
//transition指定多个属性
transition: width 2s,height 3s;
```

transition-timing-function 属性规定过渡效果的速度曲线。

```
transition-timing-function: linear|ease|ease-in|ease-out|ease-in-out
```

## 2.animation

[animation-fill-mode](http://www.w3school.com.cn/cssref/pr_animation-fill-mode.asp):属性规定动画在播放之前或之后，其动画效果是否可见

animation-timing-function:指定时间和动画进度之间的关系

```
animation-timing-function:steps(1)
//steps(n)指定关键帧之间有几个动画
```

githun上好用的动画库:

[https://daneden.github.io/animate.css/](https://daneden.github.io/animate.css/)

animation demo:

[https://gitee.com/frontendLol/cssJiaoCheng/blob/master/05-animal.html](https://gitee.com/frontendLol/cssJiaoCheng/blob/master/05-animal.html)

### 3.面试

* css动画的实现方式有几种

```
transition
keyframes(animation)
```

* 如何实现逐帧动画

```
1.使用关键帧动画
2.去掉补间(steps)
```

* css动画性能

```
1.性能不坏
2.部分情况下优于js
3.但js可以做的更好
4.部分高危属性box-shadow等
```



