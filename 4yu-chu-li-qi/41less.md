### less的设计是尽量做到和原生的css相同,无论变量的设计还是mixin的设计

### 1.嵌套

```css
div{
    background:red;
    .content{
        width:100px;
        height:100px;
        //&表示.content 同时的意义
        &:hover{
        height:200px;
        }
    }
}
```

```
//编译为
div {
  background: "red";
}
div .content {
  width: 100px;
  height: 100px;
}
div .content:hover {
  height: 200px;
}
```

### 2.变量

优势:可以避免写相同的值,可以参与计算

```css
@fontSize:12px;
@bgColor:red;
div{
    background:lighten(@bgColor,40%);
    fontsize:@fontSize+2px;
}
```

实际开始可以将一些常用的样式先定义好 eg:

```
@headFontSize:16px;
@contentFs:14px;
@textColor:#333;
@linkColor:yellow;
```

### 3.mixin ——大段代码复用

```
@fontSize:12px;
//定义一段复用的代码
.font(@fontSize){
    border:1px solid red;
    font-size: @fontSize;
}
.box{width:100px;}
.nav{
    //调用
    .box();
    .font(14px);
}
```

### 4.extend

```
.block{
    border:1px solid red;
    width:100px;
    height:100px;
}
//第一种方式
.box:extend(.block){};
//第二种方式
.content{
    &:extend(.block);
}
```

### 5.loop

```css
.gen-col(@n) when(@n>0){

    .col-@{n}{
        width:100%/12*@n;
    }

    .gen-col(@n - 1);

}
.gen-col(12);
```

### 6.import

可以将css拆分成不同的模块,用import去加载对应的css



