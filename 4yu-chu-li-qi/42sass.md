### 1.嵌套

```css
div{
    h1{
        width:100px;
        height:100px;
        background:yellow;
        //&表示h1
        &:hover{
            background:red;
        }
    }
}
```

```
//编译后
div h1 {
  width: 100px;
  height: 100px;
  background: yellow;
}

div h1:hover {
  background: red;
}
```

### 2.变量

```css
$bg:red;
$fontSize:12px;
```

### 3.mixin

* 1.使用@mixin定义代码块
* 2.使用@include引用代码块

```css
$bg:red;
@mixin bg($bg){
    background:$bg;
    line-height:40px;
    text-align: center;
}
.nav{
    @include bg(yellow);
}
```

### 4.extend

```css
.block{
    width:100px;
    height:100px;
}
.nav{
    @extend .block;
}
```

### 5.loop

```css
@mixin gen-col($n){
    @if $n > 0 {
        @include gen-col($n - 1);
        .col-#{$n}{
            width:100%/12*$n;
        }
    }
}
@include gen-col(12)
;
```

```css
推荐使用
//sass之处for循环
@for $i from 1 through 12 {
    .col-#{$i}{
        width:100%/12*$i
    }
}
```

### 6.import

可以将css拆分成不同的模块,用import去加载对应的css

