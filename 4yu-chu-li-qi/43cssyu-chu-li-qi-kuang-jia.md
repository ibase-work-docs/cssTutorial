* SASS-Compass
* Less - Lesshat/EST
* 提供现成的mixin
* 类似JS类库  封装常用功能

**一、Compass是什么？**

Sass本身只是一个编译器，Compass在它的基础上，封装了一系列有用的模块和模板，补充Sass的功能。它们之间的关系，有点像Javascript和jQuery

**二、安装**

Compass是用Ruby语言开发的，所以安装它之前，必须安装Ruby

```
gem install compass
```

**三、项目初始化**

接下来，要创建一个你的Compass项目，假定它的名字叫做myproject，那么在命令行键入

```
compass create myproject
```

```
cd myproject
```

**四、编译**

在命令行中输入

```
　compass compile
```

会将sass子目录中的scss文件，编译成css文件，保存在stylesheets子目录中

```
compass compile --output-style compressed
//编译出来的css带大量注释,使用折行命令,可以删除注释
```

**五、Compass的模块**

```

    * reset
　　* css3
　　* layout
　　* typography
　　* utilities
```



