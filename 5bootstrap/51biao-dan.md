![](/assets/table.png)1.水平表单

```css
    <h1 class="text-center popover-header">注册</h1>
    <form action="" id="myForm" class="col-6 offset-3">
        <div class="row form-group">
            <label for="" class="col-sm-2 col-form-label">用户名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <label for="" class="col-sm-2 col-form-label">密码</label>
            <div class="col-sm-10">
                <input type="password" class="form-control">
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-primary ">提交</button>
        </div>
    </form>
```

```
form
    div class="row form-group"
        label class="col-sm-2 col-form-label"
        div class="col-sm-10"
            input class="form-control"
```



