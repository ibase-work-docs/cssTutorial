```css
<table class="table table-bordered table-hover table-responsive">
            <caption class="text-center">手机品牌</caption>
            <tr class="active">
                <th>手机</th>
                <th>手机</th>
            </tr>
            <tr class="warning">
                <td>小米</td>
                <td>小米</td>
            </tr>
            <tr class="success">
                <td>小米</td>
                <td>小米</td>
            </tr>
            <tr class="danger">
                <td>小米</td>
                <td>小米</td>
            </tr>
</table>
```

```
table class="table table-bordered table-hover"
```

![](/assets/table-01.png)

boostrap为表格提供了以下几种样式

1.table:基础表格

2.table-striped:斑马线表格

3.table-bordered:带边框的表格

4.table-hover:鼠标悬停高亮的表格

5.table-condensed:紧凑型表格

6.table-responsive:响应式表格

