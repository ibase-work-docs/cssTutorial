**1、img-responsive：**

响应式图片，主要针对于响应式设计

**2、img-rounded：**

圆角图片

**3、img-circle：**

圆形图片

**4、img-thumbnail：**

缩略图片

```
//bootstrap
.img-responsive{
    display:block;
    max-width:100%;
    height:auto;
}
```

```
<div class="row">
            <div class="col-sm-4">
                <img src="images/01.png" alt="" class="img-responsive img-circle">
            </div>
            <div class="col-sm-4">
                <img src="images/01.png" alt="" class="img-responsive img-rounded">
            </div>
            <div class="col-sm-4">
                <img src="images/01.png" alt="" class="img-responsive img-thumbnail">
            </div>
</div>
```

![](/assets/images.png)

