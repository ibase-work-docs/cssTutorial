```
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        data-target弹窗
      </button>
      <button type="button" class="btn btn-danger" data-toggle="modal" id="showModal">js调出弹窗</button>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <!-- data-dismiss表示隐藏模态框 -->
              <button type="button" class="close" data-dismiss="modal" >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
```

```
//js
$("#showModal").click(function(){
            $("#exampleModal").modal();
})
```

```css
<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div class="modal-header">
                <h5 class="modal-title">Title</h5>
                <button data-dismiss="modal"></button>
            </div>
            
            <div class="modal-body">
            </div>
            
            <div class="modal-footer">
                <button data-dismiss="modal">取消</button>
                <button >更新</button>                
            </div>
        </div>
    </div>
</div>
```



