#### 1.实现原理

Bootstrap框架中的网格系统就是将容器平分成12份。

#### 2.工作原理

* 1.row必须包含在container中

```
<div class="container">
    <div class="row"></div>
</div>
```

* 2.在行\(.row\)中可以添加列\(.column\)，但列数之和不能超过平分的总列数，比如12

```
<div class="row">
            <div class="col-sm-3">
                col-3
            </div>
            <div class="col-sm-9">
                col-9
            </div>
</div>
```



