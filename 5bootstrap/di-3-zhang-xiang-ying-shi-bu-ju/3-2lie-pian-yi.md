1.偏移

```
.col-sm-offset-3
//向右偏移
```

2.排序

```
//向左偏移
.col-sm-pull-3
//向右偏移
.col-sm-push-3
```

3.嵌套

```
<div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
            //此处在嵌套一个网格
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>
            </div>
</div>   
```



