```css
<form action="" role="form" class="form-horizontal">
            <div class="form-group">
                <label for="" class="col-xs-2">用户名</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control ">
                </div>

            </div>
            <div class="form-group">
                <label for="" class="col-xs-2">密码</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox col-xs-10 col-xs-offset-2">
                    <label for="">
                        <input type="checkbox">记住密码
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-2">
                    <button type="submit" class="btn btn-default">提交</button>
                </div>
            </div>
</form>
```

```
form class="form-horizontal"
    div class="form-group"
        
```

![](/assets/table-02.png)

