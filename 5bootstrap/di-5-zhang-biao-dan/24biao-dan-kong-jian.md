为了让控件在各种表单风格中样式不出错，需要添加类名“**form-control“**

```
<form role="form">
    <div class="form-group">
        <input type="email" class="form-control" placeholder="Enter email">
    </div>
</form>
```

![](/assets/form-control.png)

