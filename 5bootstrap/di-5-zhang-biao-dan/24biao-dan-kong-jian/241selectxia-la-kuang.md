```
<form >
            <div class="form-group">
                <select class="form-control">
                    <option value="武汉">武汉</option>
                    <option value="长沙">长沙</option>
                    <option value="贵阳">贵阳</option>
                </select>
            </div>
</form>
```

```
form
    div class="form-group"
        select class="form-control"
```

![](/assets/select.png)

```
<form action="" >
            <div class="form-group">
                <select class="form-control" multiple>
                    <option value="武汉">武汉</option>
                    <option value="长沙">长沙</option>
                    <option value="贵阳">贵阳</option>
                </select>
            </div>
</form>
```

显示如下

![](/assets/多选.png)

