```
        <form action="">
            <div class="form-group">
                <textarea class="form-control" rows="5"></textarea>
            </div>
        </form>
```

```
form
    div.form-group
        textarea.form-control
```

![](/assets/textarea.png)

