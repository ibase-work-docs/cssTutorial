```
<form action="">
            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox">篮球
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox">足球
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox">羽毛球
                </label>
            </div>
</form>
```

```
form
    div.form-group
        label.checkbox-inline
```

![](/assets/inlinecb.png)

