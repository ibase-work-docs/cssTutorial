```css
        <button class="btn btn-default">default</button>
        <button class="btn btn-info">info</button>
        <button class="btn btn-primary">primary</button>
        <button class="btn btn-warning">warn</button>
        <button class="btn btn-success">success</button>
        <button class="btn btn-danger">danger</button>
        <button class="btn btn-inverse">inverse</button>
   
```

![](/assets/button.png)

```
<button class="btn btn-danger" disabled>提交</button>
```

![](/assets/disable.png)

