```
1、input-sm:让控件比正常大小更小
2、input-lg:让控件比正常大小更大
```

```
<input type="text" class="form-control input-lg">
<input type="text" class="form-control input-sm">
```

![](/assets/sl.png)

