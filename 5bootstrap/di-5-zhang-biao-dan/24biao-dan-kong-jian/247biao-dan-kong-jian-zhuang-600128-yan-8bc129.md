```
<form role="form">
            <div class="form-group has-success">
                <label class="control-label" for="inputSuccess1">成功状态</label>
                <input type="text" class="form-control" id="inputSuccess1" placeholder="成功状态">
            </div>
            <div class="form-group has-warning">
                <label class="control-label" for="inputWarning1">警告状态</label>
                <input type="text" class="form-control" id="inputWarning1" placeholder="警告状态">
            </div>
            <div class="form-group has-error">
                <label class="control-label" for="inputError1">错误状态</label>
                <input type="text" class="form-control" id="inputError1" placeholder="错误状态">
            </div>
</form>
```

```
form
    div class="form-group has-success"
        label class="control-label"
        input class="form-control"
```

![](/assets/status.png)

[https://v3.bootcss.com/components/\#glyphicons-glyphs](https://v3.bootcss.com/components/#glyphicons-glyphs)

带图片的状态框,要将fonts文件夹放到项目中

![](/assets/status-icon.png)

```
<form role="form">
            <div class="form-group has-success has-feedback">
                <label class="control-label" for="inputSuccess1">成功状态</label>
                <input type="text" class="form-control" id="inputSuccess1" placeholder="成功状态">
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </div>

</form>
```

```
form
    div class="form-group has-sucess has-feedback"
        label class="control-label"
        input class="form-control"
        span  class="form-control-feedback"
```



