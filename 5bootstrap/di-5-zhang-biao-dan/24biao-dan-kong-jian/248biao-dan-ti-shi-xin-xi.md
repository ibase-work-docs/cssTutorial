```css
<form role="form">
            <div class="form-group has-success has-feedback">
                <label for="" class="control-label">用户名</label>
                <input type="text" class="form-control">
                <span class="help-block">你输入的信息是正确的</span>
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </div>
</form>
```

![](/assets/help.png)

