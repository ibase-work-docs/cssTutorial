```
<form >
            <div class="form-group has-success has-feedback">
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" id="user">
                </div>
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </div>
</form>
```

```
.input-group
    .input-group-add
    .form-control
```

![](/assets/有图的input.png)

