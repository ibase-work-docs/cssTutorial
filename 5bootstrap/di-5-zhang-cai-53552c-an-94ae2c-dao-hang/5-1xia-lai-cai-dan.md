```
<div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button"  data-toggle="dropdown">
                手机品牌
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">苹果</a></li>
                <li><a href="#">小米</a></li>
                <li><a href="#">魅族</a></li>
            </ul>
</div>
```

```
1.给以class为dropdown的容器 <div class="dropdown"></div>
2.用btn按钮作用父菜单,定义类名dropdown-toggle，自定义属性data-toggle="dropdown"
3.下拉菜单的类名定义为dropdown-menu
```

![](/assets/dropdown.gif)

