```
<div class="dropup">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">手机品牌
            <span class="caret"></span></button>

            <ul class="dropdown-menu">
                <li class="dropdown-header">国产手机</li>
                <li><a href="#">小米</a></li>
                <li><a href="#">华为</a></li>
                <li><a href="#">vivio</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">外国品牌</li>
                <li><a href="#">苹果</a></li>
            </ul>
</div>
```

![](/assets/dropup.png)

