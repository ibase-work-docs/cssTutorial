![](/assets/vertical.png)

```html
//btn-group-vertical
<div class="btn-group-vertical">
            <button class="btn btn-default">关于我们</button>
            <button class="btn btn-default">公司介绍</button>
            <div class="btn-group">
                <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">产品<span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">电脑</a></li>
                    <li><a href="#">手机</a></li>
                    <li><a href="#">平板</a></li>
                </ul>
            </div>
        </div>
```



