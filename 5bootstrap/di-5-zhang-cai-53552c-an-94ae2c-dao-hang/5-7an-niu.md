##### 1.按钮组

![](/assets/btnGroup.png)

```html
<div class="btn-group">
            <button class="btn btn-default">
                <span class="glyphicon glyphicon-arrow-left"></span>
            </button>
            <button class="btn btn-default">
                    <span class="glyphicon glyphicon-arrow-right"></span>
            </button>
</div>
```

##### 2.按钮工具栏 btn-toolbar

![](/assets/btn-toolbar.png)

```html
<div class="btn-toolbar">
            <div class="btn-group">
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-music"></span>
                </button>
            </div>

            <div class="btn-group">
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-align-left"></span>
                </button>
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-align-center"></span>
                </button>
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-align-right"></span>
                </button>
            </div>
</div>
```



