```
.navbar .navbar-default/.navbar-inverse
    .nav .navbar-nav
```

![](/assets/navbar.png)

```html
<div class="navbar navbar-default">
      <ul class="nav navbar-nav">
            <li class="active"><a href="#">小米</a></li>
            <li><a href="#">苹果</a></li>
            <li><a href="#">华为</a></li>
      </ul>
</div>
```

navbar-inverse反色导航——背景色为黑色



