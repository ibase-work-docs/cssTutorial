![](/assets/navbar-form.png)

```html
<div class="navbar navbar-default">
                <!-- navbar-header -->
                <div class="navbar-header">
                     <a href="#" class="navbar-brand">极客营</a>
                </div>
                <!--navbar-nav  -->
                <ul class="nav navbar-nav">
                    <li><a href="#">JAVA</a></li>
                    <li><a href="#">PHP</a></li>
                    <li><a href="#">HTML5</a></li>
                </ul>
                <!-- navbar-form -->
                <form action="" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control">
                    </div>
                    <button class="btn btn-default">搜索</button>
                </form>
</div>
```



