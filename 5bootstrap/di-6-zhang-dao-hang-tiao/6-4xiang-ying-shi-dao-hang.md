![](/assets/nav-res.gif)

Tip:需要引入js文件

```js
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
```

1.将[需要折叠的内容,](/0)包裹在一个div内,并为div加入collapse,navbar-collapse两个类名

2.保证在[窄屏](/0)的时候要显示的图片\(固定写法\)

```html
<button class="navbar-toggle" type="button" data-toggle="collapse">
  <span class="sr-only">Toggle Navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>
```

3.button添加data-target=".类名/\#id名"，究竞是类名还是id名呢？由需要折叠的div来决定。

```html
<div class="navbar navbar-default">
            <div class="navbar-header">
                <!-- navbar-brand -->
                <a href="#" class="navbar-brand">极客营</a>
                <!-- navbar-toggle -->
                <button class="navbar-toggle" data-target="#collapse" data-toggle="collapse">
                    <span class="sr-only">Toggle Navbar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <!-- collapse -->
            <div class="collapse navbar-collapse" id="collapse">
                <ul class="nav navbar-nav">
                            <li><a href="#">JAVA</a></li>
                            <li><a href="#">PHP</a></li>
                            <li><a href="#">HTML5</a></li>
                 </ul> 
            </div>

</div>
```

```
.navbar .navbar-default
    .navbar-header
        .navbar-brand
        .navbar-toggle data-toggle="collapse" data-target=""
    .collpase .navbar-collapse
        .nav .navbar-nav
```



