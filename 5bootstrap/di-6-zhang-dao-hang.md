```
.nav .nav-tabs
```

#### 1.nav nav-tabs

![](/assets/nav-tabs.png)

```html
<ul class="nav nav-tabs">
            <li><a href="#">苹果</a></li>  
            <li><a href="#">小米</a></li>  
            <li><a href="#">华为</a></li>  
</ul>
```

#### 2.nav nav-pills 胶囊式导航

```html
<ul class="nav nav-pills">
            <li><a href="#">苹果</a></li>  
            <li><a href="#">小米</a></li>  
            <li><a href="#">华为</a></li>  
</ul>
```

![](/assets/nav-pills.png)

